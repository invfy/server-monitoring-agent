#!/bin/bash
#
# Invfy Agent Removal Script
#
# Set environment
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Prepare output
echo -e "|\n|   Invfy Uninstaller\n|   ===================\n|"

# Root required
if [ $(id -u) != "0" ];
then
	echo -e "|   Error: You need to be root to uninstall the Invfy agent\n|"
	exit 1
fi

# Attempt to delete previous agent
if [ -f /etc/invfy/agent.sh ]
then

	# Remove cron entry and user
	if id -u invfy >/dev/null 2>&1
	then
		# Show Feedback
		echo -e "|\n|   invfy user [FOUND]\n|"
	
		(crontab -u invfy -l | grep -v "/etc/invfy/agent.sh") | crontab -u invfy - && userdel invfy
		
		# Show Feedback
		echo -e "|\n|   invfy user & cron [DELETED]\n|"
	fi
	
	# Show Feedback
	echo -e "|\n|   /etc/invfy/agent.sh [FOUND]\n|"
	
	# Remove agent dir
	rm -Rf /etc/invfy
	
	# Show Feedback
	echo -e "|\n|   /etc/invfy [DELETED]\n|"
	
else 

	# Show Feedback
	echo -e "|\n|   /etc/invfy/agent.sh not found\n|"

fi
